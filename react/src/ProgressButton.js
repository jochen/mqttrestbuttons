import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';
import green from '@material-ui/core/colors/green';
import Button from '@material-ui/core/Button';
//import Fab from '@material-ui/core/Fab';
//import CheckIcon from '@material-ui/icons/Check';
//import SaveIcon from '@material-ui/icons/Save';

const styles = theme => ({
  root: {
    //display: 'flex',
    //alignItems: 'center',
  },
  wrapper: {
    margin: theme.spacing.unit,
    position: 'relative',
  },
  buttonSuccess: {
    backgroundColor: green[500],
    '&:hover': {
      backgroundColor: green[700],
    },
  },
  buttonProgress: {
    color: green[500],
    position: 'absolute',
    top: '50%',
    left: '50%',
    marginTop: -12,
    marginLeft: -12,
  },
});

class ProgessButton extends React.Component {
  constructor(props) {
    super(props);
    this.wsHandler = this.wsHandler.bind(this);
    //console.log(JSON.stringify(this));
  }

  state = {
    loading: false,
    success: false,
  };

  wsHandler(ev) {
    //console.log(this.constructor.name, ev);
    try {
      switch (ev.type) {
        case "open":
          //console.log(decodeddata.value);
          clearTimeout(this.timer);
          this.setState({
            loading: false,
            success: false,
          });
          break;

        case "message":
          console.log(this.constructor.name, ev);
          //console.log(ev.data);
          const decodeddata = JSON.parse(ev.data);
          switch (decodeddata.type) {
            case "position":
              //console.log(decodeddata.value);
              break;
            case "setPosition":
              //console.log(decodeddata.value);
              if (decodeddata.result === -1) {
                clearTimeout(this.timer);
                this.setState({
                  loading: false,
                  success: false,
                });
              }
              break;
            case "positionStopped":
              //console.log(decodeddata.value);
              clearTimeout(this.timer);
              this.setState({
                loading: false,
                success: false,
              });
              break;
            case "positionFinished":
              //console.log(decodeddata.value);
              if (decodeddata.value === this.props.value) {
                clearTimeout(this.timer);
                this.setState({
                  loading: false,
                  success: true,
                });
                this.timer = setTimeout(() => {
                  this.setState({
                    loading: false,
                    success: false,
                  });
                }, 5000);
              }
              else {
                clearTimeout(this.timer);
                this.setState({
                  loading: false,
                  success: false,
                });

              }
              break;
            default:
              break;

          }
          break;
        default:
          break;

      }
    } catch (e) {
      console.log(e, ev.data);
    }
  }
  componentDidMount() {
    this.props.socket.addEventListener("message", this.wsHandler);
    this.props.socket.addEventListener("open", this.wsHandler);
  }

  componentWillUnmount() {
    clearTimeout(this.timer);
    this.props.socket.removeEventListener("message", this.wsHandler);
    this.props.socket.removeEventListener("open", this.wsHandler);
  }

  handleButtonClick = () => {
    if (!this.state.loading) {
      this.props.socket.send(JSON.stringify({
        type: "setPosition",
        value: this.props.value,
      }));
      this.setState(
        {
          success: false,
          loading: true,
        },
        () => {
          this.timer = setTimeout(() => {
            this.setState({
              loading: false,
              success: false,
            });
          }, 40000);
        },
      );
    }
  };

  render() {
    const { loading, success } = this.state;
    const { children } = this.props;
    const { classes } = this.props;
    const buttonClassname = classNames({
      [classes.buttonSuccess]: success,
    });

    return (
      <div className={classes.root}>
        <div className={classes.wrapper}>
          <Button
            variant="contained"
            color="primary"
            className={buttonClassname}
            disabled={loading}
            onClick={this.handleButtonClick}
          >{children}
          </Button>
          {loading && <CircularProgress size={24} className={classes.buttonProgress} />}
        </div>
      </div>
    );
  }
}

ProgessButton.propTypes = {
  classes: PropTypes.object.isRequired,
  socket: PropTypes.object.isRequired,
};

export default withStyles(styles)(ProgessButton);