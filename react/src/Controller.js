import React, { Component } from 'react';
import ReconnectingWebSocket from 'reconnecting-websocket';
import Button from "@material-ui/core/Button";
import ProgessButton from './ProgressButton';
import SendNumberValueButton from './SendNumberValueButton';
import OnlineIndicator from './OnlineIndicator';
import { connect } from 'net';
import HintMessage from './HintMessage';
import MenuAppBar from './MenuAppBar';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import green from '@material-ui/core/colors/green';


const styles = theme => ({
    root: {
        //minHeight: '100vh',
        marginTop: 10,
        flexGrow: 1,
        height: '100%',

        //display: 'flex',
        alignItems: 'center',
    },
    wrapper: {
        //margin: theme.spacing.unit,
        position: 'center',
    },
});


/* class Controller extends Component {
    constructor(props) {
        super(props);
        this.wsHandler = this.wsHandler.bind(this);

        console.log("Controller constructor", this.state, props);
    }

    render() {
        const { classes, buttoncount, socket } = this.props;

        return (
            <div className={classes.root}>
                <Grid container spacing={24} style={{ textAlign: "center" }}>
                  
                    {
                        Array(buttoncount).fill(42).map((_, index) => {
                            return (
                                <React.Fragment key={index}>
                                    <Grid item xs={6}>
                                        <SendNumberValueButton socket={socket} color="primary" number={index} value="1" typename="button">Button {index} shortpress</SendNumberValueButton>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <SendNumberValueButton socket={socket} color="secondary" number={index} value="2" typename="button">Button {index} longpress</SendNumberValueButton>
                                    </Grid>
                                </React.Fragment>
                            )
                        })
                    }
                    <Grid item xs={12}>
                        <HintMessage socket={socket} />
                    </Grid>
                </Grid>
            </div>
        );
    }
} */

const Controller = (props) => (
    <div className={props.classes.root}>
        <Grid container spacing={24} style={{ textAlign: "center" }}>
            {
                Array(props.buttoncount).fill(42).map((_, index) => {
                    return (
                        <React.Fragment key={index}>
                            <Grid item xs={6}>
                                <SendNumberValueButton socket={props.socket} color="primary" number={index} value="1" typename="button">Button {index} shortpress</SendNumberValueButton>
                            </Grid>
                            <Grid item xs={6}>
                                <SendNumberValueButton socket={props.socket} color="secondary" number={index} value="2" typename="button">Button {index} longpress</SendNumberValueButton>
                            </Grid>
                        </React.Fragment>
                    )
                })
            }
            <Grid item xs={12}>
                <HintMessage socket={props.socket} />
            </Grid>
        </Grid>
    </div>
)



Controller.propTypes = {
    classes: PropTypes.object.isRequired,
    socket: PropTypes.object.isRequired,
};

export default withStyles(styles)(Controller);