import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ReconnectingWebSocket from 'reconnecting-websocket';
import classNames from 'classnames';
import { withStyles, withTheme } from '@material-ui/core/styles';
import MenuItem from '@material-ui/core/MenuItem';
import DebugTextField from './DebugTextField';
import grey from '@material-ui/core/colors/grey';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import SaveIcon from '@material-ui/icons/Save';
import Grid from '@material-ui/core/Grid';
import DebugSwitch from './DebugSwitch';

const styles = theme => ({
    root: {
        height: '100%',
        marginTop: 10,
        flexGrow: 1,

        //display: 'flex',
        //width: "50%",
        //height: '100%',
        //height: '100vh',
        //height: '100vmin',
        //marginTop: 1,
        //background: grey[100],
    },
    wrapper: {
        display: 'flex',
        flexWrap: 'wrap',
    },
});

class Debug extends Component {
    constructor(props) {
        super(props);
        this.wsDebugHandler = this.wsDebugHandler.bind(this);
        //this.sendForm = this.sendForm.bind(this);

        //console.log(JSON.stringify(this));
    }
    state = {
        socket: null,
        elements: {},
    };

    wsDebugHandler(ev) {
        //console.log(this.constructor.name, ev);
        try {
            switch (ev.type) {
                case "open":
                    //console.log(decodeddata.value);
                    break;

                case "message":
                    console.log("message", this.constructor.name, ev);
                    var res = JSON.parse('[' + ev.data.replace(/}{/g, '},{') + ']');
                    //console.log("message res:", res);

                    //const decodeddata = 
                    res.forEach((decodeddata) => {
                        if (decodeddata.name !== undefined) {
                            const name = decodeddata.name;
                            const elements = this.state.elements;
                            elements[name] = decodeddata;
                            this.setState({ elements: elements });
                        }
                        //console.log(this.state.elements);
                    });
                    //const decodeddata = JSON.parse(ev.data);
                    //switch (decodeddata.type) {
                    break;
                default:
                    break;
            }
        } catch (e) {
            console.log(e, ev.data);
        }
    }

    componentDidMount() {

        this.state.socket = new ReconnectingWebSocket('ws://' + this.props.wshost + '/debug', 'optionalProtocol');

        this.state.socket.addEventListener("open", this.wsDebugHandler);
        this.state.socket.addEventListener("message", this.wsDebugHandler);
        this.state.socket.addEventListener("close", this.wsDebugHandler);
        this.state.socket.addEventListener("error", this.wsDebugHandler);
    }

    componentWillUnmount() {
        this.state.socket.removeEventListener("open", this.wsDebugHandler);
        this.state.socket.removeEventListener("message", this.wsDebugHandler);
        this.state.socket.removeEventListener("close", this.wsDebugHandler);
        this.state.socket.removeEventListener("error", this.wsDebugHandler);
        this.state.socket.close();
        this.state.socket = null;
    }

    render() {
        const { elements, socket } = this.state;

        const { classes } = this.props;

        return (
            <div className={classes.root}>
                <div className={classes.wrapper}>
                    {
                        Object.keys(elements).map(function (element, index) {
                            if (elements[element].type == "text") {
                                return <DebugTextField
                                    key={index}
                                    label={element}
                                    helpertext={"Update millies: " + elements[element].millis}
                                    value={elements[element].value}
                                />;
                            }
                            if (elements[element].type == "logic") {
                                return <DebugSwitch
                                socket={socket}
                                key={index}
                                label={element}
                                helperText={"Update millies: " + elements[element].millis}
                                value={elements[element].value}
                                />;
                            }
                        })
                    }
                </div>
            </div>
        );
    }
}

Debug.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Debug);