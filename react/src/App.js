import React, { Component } from 'react';
import ReconnectingWebSocket from 'reconnecting-websocket';
import Button from "@material-ui/core/Button";
import TemperatureGauge from './TemperatureGauge';
import ProgessButton from './ProgressButton';
import OnlineIndicator from './OnlineIndicator';
import './App.css';
import { connect } from 'net';
import HintMessage from './HintMessage';
import MenuAppBar from './MenuAppBar';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Controller from './Controller';
import Config from './Config';
import { BrowserRouter as HashRouter, Route, withRouter } from 'react-router-dom';
import { HashLink as Link } from 'react-router-hash-link';
import Debug from './Debug';


class App extends Component {

  constructor(props) {
    super(props);
    this.wsHandler = this.wsHandler.bind(this);

    const searchParams = new URLSearchParams(window.location.search);
    //console.log(searchParams.get("s"));
    //this.handleClick = this.handleClick.bind(this);
    //var wshost = window.location.host;
    var wshost = searchParams.get("ws");
    if (wshost === null)
    {
      wshost = window.location.host;
    }
    this.state = {
      socket: new ReconnectingWebSocket('ws://' + wshost + '/control', 'optionalProtocol'),
      wshost,
      buttoncount: 0,
    }

    this.state.socket.addEventListener("message", this.wsHandler);
    //this.state.socket.addEventListener("message", this.ws2Handler);
    /*
    this.state.socket.addEventListener("open", this.wsHandler);
    this.state.socket.addEventListener("message", this.wsHandler);
    this.state.socket.addEventListener("close", this.wsHandler);
    this.state.socket.addEventListener("error", this.wsHandler);
    */
  }

/*   componentDidMount(){
    console.log("mount app", this.state);
    this.setState({ buttoncount: this.state.buttoncount });
    //this.state.socket.addEventListener("message", this.wsHandler);

    //console.log(props.location); // i can access the id here
  } */
/*
  handleClick(value) {
    this.state.socket.send(JSON.stringify({
      type: "buttonStop",
      value: true,
    }));

  }
*/

  wsHandler(ev) {
    try {
        switch (ev.type) {
            case "message":
                console.log(this.constructor.name, ev);
                //console.log("App", ev.data);
                const decodeddata = JSON.parse(ev.data);
                switch (decodeddata.type) {
                    case "info":
                        //console.log(decodeddata.buttoncount);
                        //console.log(this.state);
//                        this.state.buttoncount = Array.apply(null, { length: decodeddata.buttoncount }).map(Number.call, Number);
//                        this.setState({buttoncount: Array.apply(null, { length: decodeddata.buttoncount }).map(Number.call, Number),});

                        this.setState({buttoncount: 3})
                        console.log("App state", this.state);
                        break;
                    default:
                        break;
                }
                break;
            default:
                break;

        }
    } catch (e) {
        console.log(e, ev.data);
    }

  }


  render() {
    const { buttoncount } = this.state;

    return (
      <HashRouter basename="/#">
      <div className="App">
        <header className="App-header">
          <Route path="*" render={()=>(
            <MenuAppBar socket={this.state.socket} realName={this.state.wshost} />
        )}/>
          <Route exact path ="/" render={()=>(
              <Controller socket={this.state.socket} buttoncount={buttoncount}/>
        )}/>
          <Route path="/config" render={()=>(
              <Config socket={this.state.socket} buttoncount={buttoncount}/>
        )}/>
          <Route path="/debug" render={()=>(
              <Debug wshost={this.state.wshost}/>
        )}/>
        </header>
      </div>
      </HashRouter>
      );
  }
}

export default App;
