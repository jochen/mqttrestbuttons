import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { withStyles, withTheme } from '@material-ui/core/styles';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';
import grey from '@material-ui/core/colors/grey';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import SaveIcon from '@material-ui/icons/Save';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import OnlineIndicator from './OnlineIndicator';
import Fab from '@material-ui/core/Fab';
import LoopIcon from '@material-ui/icons/Loop'
import Icon from '@material-ui/core/Icon';
import DeleteIcon from '@material-ui/icons/Delete';
import { Tooltip } from '@material-ui/core';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Slide from '@material-ui/core/Slide';



//import RaisedButton from 'material-ui/RaisedButton';

const styles = theme => ({
    /*
    root: {
        display: 'inline-block',
        //width: "50%",
        height: '100vh',
        //marginTop: 1,
        background: grey[100],
    },
    container: {
        display: 'flex',
        //display: 'center',
        flexWrap: 'wrap',
    },
    */
    root: {
        height: '100%',
        marginTop: 10,
        flexGrow: 1,

        //minHeight: '100vh',
        //flexGrow: 1,
        //display: 'flex',
        //alignItems: 'center',
    },

    paper: {
        ...theme.mixins.gutters(),
        paddingTop: theme.spacing.unit * 2,
        paddingBottom: theme.spacing.unit * 2,
        maxWidth: 500,
    },

    textField: {
        //margin: 20,
        //width: '90%',
        //marginLeft: theme.spacing.unit,
        //marginRight: theme.spacing.unit,
    },
    button: {
        margin: theme.spacing.unit,
    },
    fab: {
        margin: theme.spacing.unit,
    },
    extendedIcon: {
        marginRight: theme.spacing.unit,
    },


});

function Transition(props) {
    return <Slide direction="up" {...props} />;
}

function extend(obj, src) {

    var extended = {};

    // Merge the object into the extended object
    var merge = function (obj) {
        for (var prop in obj) {
            if (obj.hasOwnProperty(prop)) {
                if (Object.prototype.toString.call(obj[prop]) === '[object Object]') {
                    // If we're doing a deep merge and the property is an object
                    extended[prop] = extend(extended[prop], obj[prop]);
                } else {
                    // Otherwise, do a regular merge
                    extended[prop] = obj[prop];
                }
            }
        }
    };

    merge(obj);
    merge(src);

    return extended;
}

function index(obj, is, value) {
    if (typeof is == 'string') {
        //console.log("split", is.split('.'));
        return index(obj, is.split('.'), value);
    }
    else if (is.length > 1 && obj[is[0]] === undefined) {
        //console.log("undef", obj[is[0]]);
        obj[is[0]] = {};
        return index(obj[is[0]], is.slice(1), value);
    }
    else if (is.length == 1 && value !== undefined) {
        //console.log("obj 0", obj[is[0]]);
        return obj[is[0]] = value;
    }
    else if (is.length == 0)
        return obj;
    else {
        //console.log("else", obj[is[0]], is.slice(1));
        return index(obj[is[0]], is.slice(1), value);
    }
}


class Config extends Component {
    constructor(props) {
        super(props);
        this.wsHandler = this.wsHandler.bind(this);
        this.sendForm = this.sendForm.bind(this);

        //console.log(JSON.stringify(this));
    }
    state = {
        buttoncount: [1],
        config: {},
        tosend: true,
        locked: true,
        getconfig: true,
        warnDialogOpen: false,
    };


    handleClickWarnDialogOpen = () => {
        this.setState({ warnDialogOpen: true });
    };

    handleWarnDialogClose = result => event => {
        this.setState({ warnDialogOpen: false });
        if (result) {
            console.log(result, event);
            this.props.socket.send(JSON.stringify({
                trigger: "wifireset",
            }));
        }
    };

    handleTriggerClick = message => event => {
        console.log(message, event);
        this.props.socket.send(JSON.stringify({
            trigger: message,
        }));

    }

    getconfigvalue = name => {
        let config_orig = this.state.config;
        let value = index(config_orig, name);
        //console.log("getconfigvalue", name, value);
        if (value === undefined) return "";
        return value;
        
    }

    handleChange = name => event => {
        let config_orig = this.state.config;
        //console.log("config_orig", config_orig);
        let newconfig = {};
        //console.log("event", name);
        index(newconfig, name, event.target.value);
        //console.log("newconfig", newconfig);
        let configset = extend(config_orig, newconfig);
        //console.log("configset", configset);
        this.setState({
            config: configset,
            tosend: false,
        });
        //console.log(this.state);
        clearTimeout(this.timer);
        event.persist();
        this.timer = setTimeout(() => {
            this.sendForm(event);
        }, 15000);

    };

    sendForm = event => {
        clearTimeout(this.timer);
        console.log("sendform");
        console.log(event);
        event.preventDefault();
        console.log(this.state);
        this.props.socket.send(JSON.stringify({
            config: this.state.config,
        }));
        this.setState({
            tosend: true,
        });

    }
    wsHandler(ev) {
        //console.log(this.constructor.name, ev);
        try {
            switch (ev.type) {
                case "open":
                    //console.log(decodeddata.value);
                    if (this.state.getconfig) {
                        this.props.socket.send(JSON.stringify({
                            get: "config",
                        }));
                        this.setState({ getconfig: false })
                    }
                    break;

                case "message":
                    //console.log(this.constructor.name, ev);
                    //console.log(ev.data);
                    const decodeddata = JSON.parse(ev.data);
                    switch (decodeddata.type) {
                        case "config":
                            console.log("decoded_config", decodeddata.config);
                            if (decodeddata.buttoncount) {
                                this.setState({ buttoncount: Array.apply(null, { length: decodeddata.buttoncount }).map(Number.call, Number) });
                            }
                            this.setState({
                                locked: false,
                                config: decodeddata.config,
                            });
                            //console.log("this.state", this.state);
                            break;
                        default:
                            break;
                    }

                    break;
                default:
                    break;
            }
        } catch (e) {
            console.log(e, ev.data);
        }
    }

    componentDidMount() {
        this.props.socket.addEventListener("message", this.wsHandler);
        this.props.socket.addEventListener("open", this.wsHandler);
        if (this.state.getconfig) {
            this.props.socket.send(JSON.stringify({
                get: "config",
            }));
            this.setState({ getconfig: false })
        }

    }

    componentWillUnmount() {
        this.props.socket.removeEventListener("message", this.wsHandler);
        this.props.socket.removeEventListener("open", this.wsHandler);
    }

    render() {
        const {
            tosend,
            locked,
            buttoncount,
            config,
        } = this.state;

        const { classes } = this.props;

        return (
            <div className={classes.root}>
                <Paper className={classes.paper} elevation={1}>
                    <form className={classes.container} noValidate autoComplete="off" onSubmit={this.sendForm}>
                        <Grid container spacing={24}>
                            {
                                Object.keys(buttoncount).map(function (element, index) {
                                    return (
                                        <React.Fragment key={element}>
                                            <Grid item xs={12}>
                                                <TextField
                                                    id="outlined-with-placeholder"
                                                    label={"Button " + element + " mqtt topic"}
                                                    placeholder="top/sub1/sub2"
                                                    className={classes.textField}
                                                    onChange={this.handleChange("button." + element + ".topic")}
                                                    value={this.getconfigvalue("button." + element + ".topic")}
                                                    helperText="Topic for button value publishing"
                                                    fullWidth
                                                    margin="normal"
                                                    variant="outlined"
                                                    disabled={locked}
                                                />
                                            </Grid>
                                            <Grid item xs={4}>
                                                <TextField
                                                    id="outlined-with-placeholder"
                                                    label="Payload value"
                                                    placeholder="100"
                                                    className={classes.textField}
                                                    onChange={this.handleChange("button." + element + ".value")}
                                                    value={this.getconfigvalue("button." + element + ".value")}
                                                    helperText={"Value for pressed Button " + element}
                                                    fullWidth
                                                    margin="normal"
                                                    variant="outlined"
                                                    disabled={locked}
                                                />
                                            </Grid>
                                            <Grid item xs={4}>
                                                <TextField
                                                    id="outlined-with-placeholder"
                                                    label="Payload longpress value"
                                                    placeholder="255"
                                                    className={classes.textField}
                                                    onChange={this.handleChange("button." + element + ".longpressvalue")}
                                                    value={this.getconfigvalue("button." + element + ".longpressvalue")}
                                                    helperText={"Value for longpressed Button " + element}
                                                    fullWidth
                                                    margin="normal"
                                                    variant="outlined"
                                                    disabled={locked}
                                                />
                                            </Grid>
                                            <Grid item xs={4}>
                                                <TextField
                                                    id="outlined-with-placeholder"
                                                    label="Longpress millis"
                                                    placeholder="2000"
                                                    className={classes.textField}
                                                    onChange={this.handleChange("button." + element + ".longpressmillis")}
                                                    value={this.getconfigvalue("button." + element + ".longpressmillis")}
                                                    helperText={"Milliseconds for longpressed " + element}
                                                    fullWidth
                                                    margin="normal"
                                                    variant="outlined"
                                                    disabled={locked}
                                                />
                                            </Grid>
                                            
                                            <Grid item xs={12}>
                                                <TextField
                                                    id="outlined-with-placeholder"
                                                    label={"Button " + element + " GET Url"}
                                                    placeholder="http://hostname/rest?toggle"
                                                    className={classes.textField}
                                                    onChange={this.handleChange("button." + element + ".geturl")}
                                                    value={this.getconfigvalue("button." + element + ".geturl")}
                                                    helperText="URL for Button pressed action"
                                                    fullWidth
                                                    margin="normal"
                                                    variant="outlined"
                                                    disabled={locked}
                                                />
                                            </Grid>
                                            <Grid item xs={12}>
                                                <TextField
                                                    id="outlined-with-placeholder"
                                                    label={"Button " + element + " GET Url longpressed"}
                                                    placeholder="http://hostname/rest?set=2"
                                                    className={classes.textField}
                                                    onChange={this.handleChange("button." + element + ".geturllongpress")}
                                                    value={this.getconfigvalue("button." + element + ".geturllongpress")}
                                                    helperText="URL for Button longpressed action"
                                                    fullWidth
                                                    margin="normal"
                                                    variant="outlined"
                                                    disabled={locked}
                                                />
                                            </Grid>
                                        </React.Fragment>
                                    )
                                }, this)}
                            <Grid item xs={8}>
                                <TextField
                                    id="outlined-with-placeholder"
                                    label="Firmware topic"
                                    placeholder="firmware/mqttrestbuttons"
                                    className={classes.textField}
                                    onChange={this.handleChange("firmwaretopic")}
                                    value={this.getconfigvalue("firmwaretopic")}
                                    helperText="Topic of the firmware for updates"
                                    fullWidth
                                    margin="normal"
                                    variant="outlined"
                                    disabled={locked}
                                />
                            </Grid>
                            <Grid item xs={4}>
                                <TextField
                                    id="outlined-with-placeholder"
                                    label="LED Brightness"
                                    placeholder="80"
                                    className={classes.textField}
                                    onChange={this.handleChange("led_brightness")}
                                    value={this.getconfigvalue("led_brightness")}
                                    helperText="LED Brightness 0-255"
                                    fullWidth
                                    margin="normal"
                                    variant="outlined"
                                    disabled={locked}
                                />
                            </Grid>
                            <Grid item xs={12} justify="center" container alignItems="center">
                                <Button
                                    variant="contained"
                                    size="small"
                                    className={classes.button}
                                    type="submit"
                                    label="Save"
                                    primary="true"
                                    disabled={tosend}
                                >
                                    <SaveIcon className={classNames(classes.leftIcon, classes.iconSmall)} />
                                    Save
                                </Button>
                            </Grid>
                            <Grid item container xs={4} justify="center" alignItems="center">
                                <Typography variant="button" color="secondary" >
                                    Reset Wifi
                                </Typography>
                                <Tooltip title="Reset Wifi-Settings and restart">
                                    <div>
                                        <Fab
                                            color="secondary"
                                            size="small"
                                            aria-label="Delete"
                                            className={classes.fab}
                                            onClick={this.handleClickWarnDialogOpen}
                                            //disabled={locked}
                                        >
                                            <DeleteIcon />
                                        </Fab>
                                    </div>
                                </Tooltip>

                                <Dialog
                                    open={this.state.warnDialogOpen}
                                    TransitionComponent={Transition}
                                    keepMounted
                                    onClose={this.handleWarnDialogClose}
                                    aria-labelledby="alert-dialog-slide-title"
                                    aria-describedby="alert-dialog-slide-description"
                                >
                                    <DialogTitle id="alert-dialog-slide-title">
                                        {"Sure, you are resetting Wifi?"}
                                    </DialogTitle>
                                    <DialogContent>
                                        <DialogContentText id="alert-dialog-slide-description">
                                            Resetting of Wifi brings the Device in AP Mode und must be configured.
                                        </DialogContentText>
                                    </DialogContent>
                                    <DialogActions>
                                        <Button onClick={this.handleWarnDialogClose(false)} color="primary">
                                            Cancel the reset
                                        </Button>
                                        <Button onClick={this.handleWarnDialogClose(true)} color="primary">
                                            Yes, reset and restart.
                                        </Button>
                                    </DialogActions>
                                </Dialog>


                            </Grid>
                            <Grid item container xs={4} justify="center" alignItems="center">
                                <Typography variant="button" color="secondary" >
                                    Reset App Config
                                </Typography>
                                <Tooltip title="Reset App Config">
                                    <div>
                                        <Fab
                                            color="secondary"
                                            size="small"
                                            aria-label="Delete"
                                            className={classes.fab}
                                            onClick={this.handleClickWarnDialogOpen}
                                            //disabled={locked}
                                        >
                                            <DeleteIcon />
                                        </Fab>
                                    </div>
                                </Tooltip>

                                <Dialog
                                    open={this.state.warnDialogOpen}
                                    TransitionComponent={Transition}
                                    keepMounted
                                    onClose={this.handleWarnDialogClose}
                                    aria-labelledby="alert-dialog-slide-title"
                                    aria-describedby="alert-dialog-slide-description"
                                >
                                    <DialogTitle id="alert-dialog-slide-title">
                                        {"Sure, you are resetting App Configuration?"}
                                    </DialogTitle>
                                    <DialogContent>
                                        <DialogContentText id="alert-dialog-slide-description">
                                            Resetting of App Configuration, delete the Config only from EEPROM.
                                        </DialogContentText>
                                    </DialogContent>
                                    <DialogActions>
                                        <Button onClick={this.handleWarnDialogClose(false)} color="primary">
                                            Cancel the reset
                                        </Button>
                                        <Button onClick={this.handleWarnDialogClose(true)} color="primary">
                                            Yes, reset.
                                        </Button>
                                    </DialogActions>
                                </Dialog>


                            </Grid>
                            <Grid item container xs={4} justify="center" alignItems="center">
                                <Typography variant="button" color="primary" >
                                    Restart
                                </Typography>
                                <Tooltip title="Restart (Reset) Controller">
                                    <div>
                                        <Fab
                                            color="primary"
                                            size="small"
                                            aria-label="Delete"
                                            className={classes.fab}
                                            onClick={this.handleTriggerClick("restart")}
                                            //disabled={locked}
                                        >
                                            <LoopIcon />
                                        </Fab>
                                    </div>
                                </Tooltip>
                            </Grid>

                        </Grid>
                    </form>
                </Paper>
            </div>
        );
    }
}

Config.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Config);
