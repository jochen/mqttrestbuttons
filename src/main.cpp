#include <Arduino.h>
#include <EEPROM.h>
#include "Updater.h"
#include <ArduinoJson.h>
#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <string>
#include <map>
#include "config.h"

//#include "ESPAsyncTCP.h"
#include <ESPAsyncWebServer.h>
#include <ESPAsyncWiFiManager.h>
#include <AsyncMqttClient.h>
#include <FastLED.h>

extern "C" {
  #include "user_interface.h"
}

#define ENABLE_PRINT

#ifndef ENABLE_PRINT
// disable Serial output
#define Serial SerialDummy
static class
{
public:
  void begin(...) {}
  void print(...) {}
  void println(...) {}
  void setDebugOutput(...) {}
  void flush(...) {}
  void printf(...) {}
} Serial;

#endif

#define DEBUGOUT // Comment to enable debug output

#define DBG_OUTPUT Serial

#ifdef DEBUGOUT
#define DEBUGLOG(...) DBG_OUTPUT.printf(__VA_ARGS__)
#else
#define DEBUGLOG(...)
#endif

extern void handleFileUpload(AsyncWebServerRequest *request, String filename, size_t index, uint8_t *data, size_t len, bool final);
extern void handleFileDelete(AsyncWebServerRequest *request);
extern void updateFirmware(AsyncWebServerRequest *request, String filename, size_t index, uint8_t *data, size_t len, bool final);
extern void handleFileList(AsyncWebServerRequest *request);
extern void handleFileRead(AsyncWebServerRequest *request);

extern void sendDebugWS(String name, String type, String value);

#define DEFAULT_HOSTNAME "buttons"
#define MDNSSERVICENAME DEFAULT_HOSTNAME

#define XSTR(x) STR(x)
#define STR(x) #x

#ifndef BUILDTIME
#define BUILDTIME "none"
#endif
#pragma message("BUILDTIME=" XSTR(BUILDTIME))


int wakeup_reason;
bool doing_update = false;
bool force_config = false;
bool do_restart = false;
long rssi;
int do_subscripe_firmwaretopic_id = 0;

const char *version = "0.1";
const char *buildtime = (const char *)BUILDTIME;

WiFiEventHandler stationModeGotIpHandler;

void *jsonMessage;

AsyncMqttClient mqttClient;
//TimerHandle_t mqttReconnectTimer;
//TimerHandle_t wifiReconnectTimer;

AsyncWebServer server(80);
AsyncWebSocket wscontrol("/control");
DNSServer dns;

extern unsigned long crc(uint8_t *blob, unsigned int size);

hostConfig myhostConfig = {};

appConfig myappConfig = {};
bool has_valid_appconfig = false;

extern void setupApp();
extern void loopApp();

extern void setupDebug();
extern void loopDebug();

extern int button(int number, int value);

std::map<std::string, std::function<int(int,int)>> wsMap =
    {
        {"button", button}
};




unsigned long noWifiRestartTimeout = 0;

//flag for saving data
bool shouldSaveConfig = false;
//callback notifying us of the need to save config
void saveConfigCallback()
{
  Serial.println(F("Save config\n"));
  shouldSaveConfig = true;
}

void onNotFound(AsyncWebServerRequest *request)
{
  //Handle Unknown Request
  request->send(404);
}

void broadcastWS(String type, int value)
{
  wscontrol.printfAll("{ \"type\": \"%s\", \"value\": %d }", type.c_str(), value);
}


/*
Method to print the reason by which ESP32
has been awaken from sleep
*/
void print_wakeup_reason()
{
  
  struct rst_info *rstInfo = system_get_rst_info();
  wakeup_reason = rstInfo->reason;

  /* 
  REANSON_DEFAULT_RST = 0, // normal startup by power on
  REANSON_WDT_RST = 1, // hardware watch dog reset
  REANSON_EXCEPTION_RST = 2, // exception reset, GPIO status won't change
  REANSON_SOFT_WDT_RST = 3, // software watch dog reset, GPIO status won't change
  REANSON_SOFT_RESTART = 4, // software restart ,system_restart , GPIO status won't change
  REANSON_DEEP_SLEEP_AWAKE = 5, // wake up from deep-sleep
  */

  switch (wakeup_reason)
  {
  case 0:
    Serial.println("Wakeup caused power on");
    break;
  case 1:
    Serial.println("Wakeup caused hardware WDT reset");
    break;
  case 2:
    Serial.println("Wakeup caused exception reset");
    break;
  case 3:
    Serial.println("Wakeup caused software WDT reset");
    break;
  case 4:
    Serial.println("Wakeup caused soft resetart");
    break;
  case 5:
    Serial.println("Wakeup caused by timer");
    break;
  default:
    Serial.printf("Wakeup cause unknown: %d\n", wakeup_reason);
    break;
  }
}


void connectToMqtt()
{
  if (!mqttClient.connected())
  {
    Serial.println("Connecting to MQTT...");
    mqttClient.setMaxTopicLength(MAXTOPICLENGTH);
    mqttClient.connect();
  }
  else
  {
    Serial.println("MQTT already connected!");
  }
}

void stationModeGotIp(const WiFiEventStationModeGotIP& event)
{
    Serial.println("WiFi connected");
    Serial.println("IP address: ");
    Serial.println(WiFi.localIP());
    rssi = WiFi.RSSI();
    Serial.print("RSSI:");
    Serial.println(rssi);
    connectToMqtt();

}


/*   uint16_t packetIdPub = mqttClient.publish(
      String(topicPrefix + "/button_" + button).c_str(), 1, false,
      String((uint8_t) value).c_str());
  Serial.print("Publishing at QoS 1, packetId: ");
  Serial.println(packetIdPub);
  //last_mqtt_package_id = packetIdPub;
  return packetIdPub;
} */

uint16_t mqttPublish(String topic, String payload)
{
  //uint16_t publish(const char* topic, uint8_t qos, bool retain, const char* payload = nullptr, size_t length = 0, bool dup = false, uint16_t message_id = 0)
  return mqttClient.publish(topic.c_str(), 2, false, payload.c_str());
}

uint16_t publishStatus()
{
  String topicPrefix = String(myhostConfig.configTopic);

  StaticJsonDocument<200> doc;
  doc["rssi"] = rssi;
  doc["version"] = version;
  doc["buildtime"] = buildtime;
  doc["wakeup_reason"] = (uint8_t) wakeup_reason;

  String payload;
  serializeJson(doc, payload);
  uint16_t packetIdPub1 = mqttClient.publish(
      String(topicPrefix + "/status").c_str(), 1, true,
      payload.c_str());
  Serial.print("Publishing at QoS 1, packetId: ");
  Serial.println(packetIdPub1);
  return packetIdPub1;
}

void onMqttConnect(bool sessionPresent)
{
  Serial.println("Connected to MQTT.");
  Serial.print("Session present: ");
  Serial.println(sessionPresent);

  String topicPrefix = String(myhostConfig.configTopic);

  uint16_t packetIdSub2 = mqttClient.subscribe(
      String(topicPrefix + "/config").c_str(), 2);
  Serial.print("Subscribing at QoS 2, packetId: ");
  Serial.println(packetIdSub2);

  uint16_t packetIdSub3 = mqttClient.subscribe(
      String(topicPrefix + "/trigger").c_str(), 2);
  Serial.print("Subscribing at QoS 2, packetId: ");
  Serial.println(packetIdSub3);

  //last_mqtt_package_id = 
  publishStatus();

/*   if (myappConfig.crc != 0)
  {
    for (uint8_t button = 0; button < BUTTONCOUNT; button++)
    {
      if (buttonPressed[button] && String(myappConfig.buttons[button].Topic) != "")
      {
        last_mqtt_package_id = publishButton(button, BUTTON_STATE::SHORT_PRESSED);
      }
    }
  }
 */
}

void onMqttSubscribe(uint16_t packetId, uint8_t qos)
{
  Serial.println("Subscribe acknowledged.");
  Serial.print("  packetId: ");
  Serial.println(packetId);
  Serial.print("  qos: ");
  Serial.println(qos);
}

void onMqttUnsubscribe(uint16_t packetId)
{
  Serial.println("Unsubscribe acknowledged.");
  Serial.print("  packetId: ");
  Serial.println(packetId);
}

void resetAppConfig()
{
  Serial.println("Reset App Config");
  for (int i = sizeof(hostConfig) ; i < sizeof(hostConfig) + sizeof(appConfig) ; i++) {
    EEPROM.write(i, 0);
  }
  EEPROM.commit();
  has_valid_appconfig = false;  
}

void saveAppConfig()
{
  unsigned long newcrc = crc((uint8_t *)&myappConfig, sizeof(appConfig));
  if (newcrc != myappConfig.crc)
  {
    Serial.println("Save new App Config");
    myappConfig.crc = newcrc;
    EEPROM.put(sizeof(hostConfig), myappConfig);
    EEPROM.commit();
    Serial.println(myappConfig.crc);
    has_valid_appconfig = true;
  }
}

void processJsonConfig(JsonObject rootobj)
{
  /* String obj;
  serializeJson(rootobj, obj);
  Serial.println(obj);  
  return; */
  if (rootobj.containsKey("led_brightness"))
  {
    uint8_t led_brightness = rootobj["led_brightness"];
    Serial.print("set new led brightness config: ");
    Serial.println(led_brightness);
    myappConfig.led_brightness = led_brightness;
  }
  
  if (rootobj.containsKey("firmwaretopic"))
  {
    String firmwaretopic = rootobj["firmwaretopic"];
    if (firmwaretopic.length() < MAXTOPICLENGTH)
    {
      Serial.print("set new firmware topic from config: ");
      Serial.println(firmwaretopic);
      strcpy(myappConfig.firmwaretopic, firmwaretopic.c_str());
    }
  }
  if (rootobj.containsKey("button"))
  {
    Serial.println("Contains Key button");
    JsonObject buttons = rootobj["button"];
    for (uint8_t button = 0; button < BUTTONCOUNT; button++)
    {
      if (buttons.containsKey(String(button)))
      {
        Serial.print("Contains Button: ");
        Serial.println(button);
        JsonObject buttonConf = buttons[String(button)];
        if (buttonConf.containsKey("topic"))
        {
          strcpy(myappConfig.buttons[button].Topic, buttonConf["topic"]);
          Serial.print("topic:");
          Serial.println(myappConfig.buttons[button].Topic);
        }
        if (buttonConf.containsKey("value"))
        {
          strcpy(myappConfig.buttons[button].Value, buttonConf["value"]);
          Serial.print("value:");
          Serial.println(myappConfig.buttons[button].Value);
        }
        if (buttonConf.containsKey("geturl"))
        {
          strcpy(myappConfig.buttons[button].getUrl, buttonConf["geturl"]);
          Serial.print("geturl:");
          Serial.println(myappConfig.buttons[button].getUrl);
        }
        if (buttonConf.containsKey("geturllongpress"))
        {
          strcpy(myappConfig.buttons[button].getUrlLongpress, buttonConf["geturllongpress"]);
          Serial.print("geturllongpress:");
          Serial.println(myappConfig.buttons[button].getUrlLongpress);
        }
        if (buttonConf.containsKey("longpressvalue"))
        {
          strcpy(myappConfig.buttons[button].LongpressValue, buttonConf["longpressvalue"]);
          Serial.print("long value:");
          Serial.println(myappConfig.buttons[button].LongpressValue);
        }
        if (buttonConf.containsKey("longpressmillis"))
        {
          strcpy(myappConfig.buttons[button].LongpressMillis, buttonConf["longpressmillis"]);
          Serial.print("long time:");
          Serial.println(myappConfig.buttons[button].LongpressMillis);
        }
      
      }
    }
  }
  saveAppConfig();
}

void onMqttMessage(char *topic, char *payload, AsyncMqttClientMessageProperties properties, size_t len, size_t index, size_t total)
{
  Serial.print("Publish received, ");
  Serial.print("  topic: ");
  Serial.print(topic);
  Serial.print(",  qos: ");
  Serial.print(properties.qos);
  Serial.print(",  dup: ");
  Serial.print(properties.dup);
  Serial.print(",  retain: ");
  Serial.print(properties.retain);
  Serial.print(",  len: ");
  Serial.print(len);
  Serial.print(",  index: ");
  Serial.print(index);
  Serial.print(",  total: ");
  Serial.print(total);
  if (len < 20)
  {
    Serial.print("  payload: ");

    char s_payload[len + 1];
    s_payload[len] = '\0';
    strncpy(s_payload, payload, len);
    Serial.println(s_payload);
  }
  else
  {
    Serial.println("  payload len>60");
    /*
    char s_payload[len+1];
    s_payload[len] = '\0';
    strncpy(s_payload, payload, len);
    */
  }

  String topicPrefix = String(myhostConfig.configTopic);

  if (String(topic) == String(topicPrefix + "/config") && total > 0)
  {
    if (index == 0)
    {
      jsonMessage = malloc(total + 1);
      memset(jsonMessage + total, 0, 1);
      //jsonMessage[total + 1] = '\0';
    }
    memcpy(jsonMessage + index, payload, len);
    if (index + len == total)
    {
      //DynamicJsonBuffer jsonBuffer;
      DynamicJsonDocument root(total + 128);
      //JsonObject &root = jsonBuffer.parseObject((char *)jsonMessage);
      auto jsonerror = deserializeJson(root, jsonMessage);
      if (!jsonerror)
      {
        Serial.println("JSON parse success");
        JsonObject rootobj = root.as<JsonObject>();
        processJsonConfig(rootobj);
      }

      else
      {
        Serial.print("JSON parse failed: ");
        Serial.println(jsonerror.c_str());
      }
    }
    free(jsonMessage);
  }

  if (String(topic) == String(topicPrefix + "/trigger") && total > 0)
  {
    doing_update = true;
    char s_payload[len + 1];
    s_payload[len] = '\0';
    strncpy(s_payload, payload, len);

    uint16_t packetIdPub1 = mqttClient.publish(
        String(topicPrefix + "/trigger").c_str(), 2, true);
    Serial.print("Publishing at QoS 1, packetId: ");
    Serial.println(packetIdPub1);

    if (String(s_payload) == String("forceconfig"))
    {
      force_config = true;
      resetAppConfig();
    }
    if (String(s_payload) == String("resetappconfig"))
    {
      resetAppConfig();
    }
    if (String(s_payload) == String("firmwareupdate") &&
        String(myappConfig.firmwaretopic) != "")
    {
      doing_update = true;
      do_subscripe_firmwaretopic_id = packetIdPub1;
    }
  }

  if (String(myappConfig.firmwaretopic) != "" &&
      String(topic) == String(myappConfig.firmwaretopic) &&
      total > 0)
  {
    doing_update = true;
    int _lastError = 0;
    // start update
    if (index == 0)
    {
      //Update.runAsync(true);
      if (!Update.begin(total, U_FLASH))
      {
        _lastError = Update.getError();
#ifdef ENABLE_PRINT
        Update.printError(Serial);
#endif
        // ignore the rest
        topic[0] = 0;
      }
    }
    // do update in chunks
    if (!_lastError)
    {
      if (Update.write((uint8_t *)payload, len) != len)
      {
        _lastError = Update.getError();
#ifdef ENABLE_PRINT
        Update.printError(Serial);
#endif
        // ignore the rest
        topic[0] = 0;
      }
    }
    // finish update
    if (!_lastError)
    {
      if (index + len == total)
      {
        if (!Update.end())
        {
          _lastError = Update.getError();
#ifdef ENABLE_PRINT
          Update.printError(Serial);
#endif
        }
        else
        {
          // restart?
          //ESP.restart();
          doing_update = false;
          do_restart = true;
        }
      }
    }
    else
    {
      doing_update = false;
    }
  }
}

void onMqttPublish(uint16_t packetId)
{
  Serial.println("Publish acknowledged.");
  Serial.print("  packetId: ");
  Serial.println(packetId);
  //last_publish_success_id = packetId;
  
  if (do_subscripe_firmwaretopic_id == packetId &&
      String(myappConfig.firmwaretopic) != "")
  {
    uint16_t packetIdSub1 = mqttClient.subscribe(
        String(myappConfig.firmwaretopic).c_str(), 2);
    Serial.print("Subscribing at QoS 2, packetId: ");
    Serial.println(packetIdSub1);
    do_subscripe_firmwaretopic_id = 0;
    //last_mqtt_package_id = 0;
  }
}

void wsEvent(AsyncWebSocket *server, AsyncWebSocketClient *client, AwsEventType type, void *arg, uint8_t *data, size_t len)
{
  Serial.println(F("wsEvent"));
  noWifiRestartTimeout = millis();
  //Handle WebSocket event
  if (type == WS_EVT_CONNECT)
  {
    //client connected
    Serial.printf("ws[%s][%u] connect\n", server->url(), client->id());
    //client->printf("Hello Client %u :)", client->id());
    client->ping();
    DynamicJsonDocument doc(100);
    //StaticJsonDocument<100> doc;
    doc["type"] = "info";
    doc["realname"] = myhostConfig.realName;
    doc["buttoncount"] = BUTTONCOUNT;
    String to_send = "";
    serializeJson(doc, to_send);
    Serial.println(to_send);
    client->text(to_send);
    Serial.println("info sended");
  }
  else if (type == WS_EVT_DISCONNECT)
  {
    //client disconnected
    Serial.printf("ws[%s][%u] disconnect\n", server->url(), client->id());
  }
  else if (type == WS_EVT_ERROR)
  {
    //error was received from the other end
    Serial.printf("ws[%s][%u] error(%u): %s\n", server->url(), client->id(), *((uint16_t *)arg), (char *)data);
  }
  else if (type == WS_EVT_PONG)
  {
    //pong message was received (in response to a ping request maybe)
    Serial.printf("ws[%s][%u] pong[%u]: %s\n", server->url(), client->id(), len, (len) ? (char *)data : "");
  }
  else if (type == WS_EVT_DATA)
  {
    //data packet
    AwsFrameInfo *info = (AwsFrameInfo *)arg;
    if (info->final && info->index == 0 && info->len == len)
    {
      //the whole message is in a single frame and we got all of it's data
      Serial.printf("ws[%s][%u] %s-message[%llu]: \n", server->url(), client->id(), (info->opcode == WS_TEXT) ? "text" : "binary", info->len);
      if (info->opcode == WS_TEXT)
      {
        {
          char s_data[len + 1];
          s_data[len] = '\0';
          strncpy(s_data, (char *)data, len);
          Serial.printf("s_data:%s\n", s_data);
        }

        DynamicJsonDocument root(len + 128);
        //JsonObject &root = jsonBuffer.parseObject((char *)jsonMessage);
        auto jsonerror = deserializeJson(root, data);
        if (!jsonerror)
        {
          Serial.println("JSON parse success");
          JsonObject rootobj = root.as<JsonObject>();
          if (rootobj.containsKey("type"))
          {
            const char *wstype = root["type"];
            Serial.printf("wstype: %s\n", wstype);
            if (wstype && wsMap.count(wstype) == 1)
            {
              int number = root["number"];
              int value = root["value"];
              int wsres = wsMap[wstype](number,value);
              Serial.printf("button: %i, value: %i, wsres: %i\n", number, value, wsres);
              client->text(String(String("{ \"type\": \"") + wstype + "\", \"result\": " + String(wsres) + " }"));
            }
          }
          else if (rootobj.containsKey("config"))
          {
            Serial.println("Contains Key config");
            processJsonConfig(rootobj["config"]);

          }
          else if (rootobj.containsKey("get"))
          {
            if (rootobj["get"] == String("config"))
            {
              Serial.println("get config");
              String config_serial;

              DynamicJsonDocument doc(sizeof(appConfig) + 128);
              //StaticJsonDocument<200> doc;
              doc["type"] = "config";
              doc["buttoncount"] = BUTTONCOUNT;
              JsonObject config = doc.createNestedObject("config");
/*               config["mqtthost"] = myappConfig.mqttHost;
              config["mqtttopicprefix"] = myappConfig.mqttTopicPrefix;
              config["hostname"] = myhostConfig.hostName;
              config["realname"] = myhostConfig.realName;
              config["maxtemperature"] = myappConfig.maxTemperature;
 */
              if(has_valid_appconfig)
              {
                config["led_brightness"] = myappConfig.led_brightness;
                config["firmwaretopic"] = myappConfig.firmwaretopic;
                JsonObject buttons = config.createNestedObject("button");
                for (uint8_t button = 0; button < BUTTONCOUNT; button++)
                {
                  JsonObject buttondoc = buttons.createNestedObject(String(button));
                  buttondoc["topic"] = myappConfig.buttons[button].Topic;
                  buttondoc["value"] = myappConfig.buttons[button].Value;
                  buttondoc["geturl"] = myappConfig.buttons[button].getUrl;
                  buttondoc["geturllongpress"] = myappConfig.buttons[button].getUrlLongpress;
                  buttondoc["longpressvalue"] = myappConfig.buttons[button].LongpressValue;
                  buttondoc["longpressmillis"] = myappConfig.buttons[button].LongpressMillis;
                }
              }
              config_serial = "";
              serializeJson(doc, config_serial);
              Serial.println(config_serial);
              client->text(config_serial);
              Serial.println("config_serial sended");
              broadcastWS("mqttconnection", (int)mqttClient.connected());
              Serial.println("mqttconnection sended");
            }
          }
          else if (rootobj.containsKey("trigger"))
          {
            if (rootobj["trigger"] == String("wifireset"))
            {
              force_config = true;
            }
            else if (rootobj["trigger"] == String("restart"))
            {
              do_restart = true;
            }
            else if (rootobj["trigger"] == String("appreset"))
            {
              resetAppConfig();
            }
          }
        }
      }
      else
      {
        for (size_t i = 0; i < info->len; i++)
        {
          Serial.printf("%02x ", data[i]);
        }
        Serial.printf("\n");
      }
      /* // websocket debugging
      if (info->opcode == WS_TEXT)
        client->text("I got your text message");
      else
        client->binary("I got your binary message");
      */
    }
    else
    {
      //message is comprised of multiple frames or the frame is split into multiple packets
      if (info->index == 0)
      {
        if (info->num == 0)
          Serial.printf("ws[%s][%u] %s-message start\n", server->url(), client->id(), (info->message_opcode == WS_TEXT) ? "text" : "binary");
        Serial.printf("ws[%s][%u] frame[%u] start[%llu]\n", server->url(), client->id(), info->num, info->len);
      }

      Serial.printf("ws[%s][%u] frame[%u] %s[%llu - %llu]: ", server->url(), client->id(), info->num, (info->message_opcode == WS_TEXT) ? "text" : "binary", info->index, info->index + len);
      if (info->message_opcode == WS_TEXT)
      {
        data[len] = 0;
        Serial.printf("%s\n", (char *)data);
      }
      else
      {
        for (size_t i = 0; i < len; i++)
        {
          Serial.printf("%02x ", data[i]);
        }
        Serial.printf("\n");
      }

      if ((info->index + len) == info->len)
      {
        Serial.printf("ws[%s][%u] frame[%u] end[%llu]\n", server->url(), client->id(), info->num, info->len);
        if (info->final)
        {
          Serial.printf("ws[%s][%u] %s-message end\n", server->url(), client->id(), (info->message_opcode == WS_TEXT) ? "text" : "binary");
          if (info->message_opcode == WS_TEXT)
            client->text("I got your text message");
          else
            client->binary("I got your binary message");
        }
      }
    }
  }
  Serial.println("ws event end");

}

void addFileToWWW(String path)
{
  Serial.printf("Add File to WWW: %s\n", path.substring(2).c_str());
  server.on(path.substring(2).c_str(), HTTP_GET, [](AsyncWebServerRequest *request) {
    handleFileRead(request);
  });
}
void delFileFromWWW(String path)
{
  Serial.printf("Del File from WWW: %s\n", path.substring(2).c_str());
  auto delhandler = server.on(path.substring(2).c_str(), [](AsyncWebServerRequest *request) {
    handleFileRead(request);
  });
  //don't need handler anymore remove it
  server.removeHandler(&delhandler);
}

void scanWWWDir()
{
  Dir dir = SPIFFS.openDir("/w");
  while (dir.next())
  {
    String fileName = dir.fileName();
    addFileToWWW(fileName);
  }
}


bool setupWifiAndConfig(bool force)
{
  char get_hostname[40] = DEFAULT_HOSTNAME;
  char get_realname[40] = DEFAULT_HOSTNAME;
  char get_ip[20] = "";
  char get_gw[20] = "";
  char get_sn[20] = "";
  char get_mqttHost[20] = "";
  char get_mqttPort[10] = "";
  char get_configTopic[50] = "";

  AsyncWiFiManagerParameter set_hostname("Hostname", "ESP Hostname", get_hostname, 40);
  AsyncWiFiManagerParameter set_realname("Realname", "Realname", get_realname, 40);
  AsyncWiFiManagerParameter set_ip("IP", "ESP IP", get_ip, 20);
  AsyncWiFiManagerParameter set_gw("GW", "Gateway", get_gw, 20);
  AsyncWiFiManagerParameter set_sn("NM", "Netmasq", get_sn, 20);
  AsyncWiFiManagerParameter set_mqttHost("MQTT IP", "MQTT IP", get_mqttHost, 20);
  AsyncWiFiManagerParameter set_mqttPort("MQTT Port", "MQTT PORT 1883", get_mqttPort, 10);
  AsyncWiFiManagerParameter set_configTopic("Topic", "Topic with Configuration", get_configTopic, 50);

  AsyncWiFiManager wifiManager(&server, &dns);

  wifiManager.setConfigPortalTimeout(300);
  //set config save notify callback
  wifiManager.setSaveConfigCallback(saveConfigCallback);

  wifiManager.addParameter(&set_hostname);
  wifiManager.addParameter(&set_realname);
  wifiManager.addParameter(&set_ip);
  wifiManager.addParameter(&set_gw);
  wifiManager.addParameter(&set_sn);
  wifiManager.addParameter(&set_mqttHost);
  wifiManager.addParameter(&set_mqttPort);
  wifiManager.addParameter(&set_configTopic);

  bool success = false;
  if (force)
  {
    wifiManager.resetSettings();
    success = wifiManager.startConfigPortal("OnDemandAP");
  }
  else
  {
    wifiManager.setSTAStaticIPConfig(IPAddress(myhostConfig._ip), 
      IPAddress(myhostConfig._gw), 
      IPAddress(myhostConfig._sn), 
      IPAddress(myhostConfig._gw) //DNS
    );
    success = wifiManager.autoConnect(myhostConfig.hostName);
  }
  if (success && shouldSaveConfig)
  {
    strcpy(myhostConfig.hostName, set_hostname.getValue());
    strcpy(myhostConfig.realName, set_realname.getValue());
    IPAddress ip;
    ip.fromString(String(set_ip.getValue()));
    myhostConfig._ip = (ip_addr_t) ip;//ip.fromString(String(set_ip.getValue()));
    ip.fromString(String(set_gw.getValue()));
    myhostConfig._gw = (ip_addr_t) ip;//.fromString(String(set_gw.getValue()));
    ip.fromString(String(set_sn.getValue()));
    myhostConfig._sn = (ip_addr_t) ip;//.fromString(String(set_sn.getValue()));
    ip.fromString(String(set_mqttHost.getValue()));
    myhostConfig.mqttHost = (ip_addr_t) ip;//.fromString(String(set_mqttHost.getValue()));
    myhostConfig.mqttPort = atoi(set_mqttPort.getValue());
    strcpy(myhostConfig.configTopic, set_configTopic.getValue());

    myhostConfig.crc = crc((uint8_t *)&myhostConfig, sizeof(hostConfig));
    EEPROM.put(0, myhostConfig);
    EEPROM.commit();
    Serial.println(myhostConfig.crc);
    do_restart = true;
  }

  return success;
}

void setup()
{
  Serial.begin(74880);
  Serial.setDebugOutput(true);

  EEPROM.begin(sizeof(hostConfig) + sizeof(appConfig));

  Serial.print("Version: ");
  Serial.print(version);
  Serial.print(" ");
  Serial.println(buildtime);

  EEPROM.get(0, myhostConfig);
  EEPROM.get(sizeof(hostConfig), myappConfig);

  if (myappConfig.crc == crc((uint8_t *)&myappConfig, sizeof(appConfig)))
  {
    Serial.println(F("Valid APP Config readed"));
    Serial.println(myappConfig.crc);
    for (uint8_t button = 0; button < BUTTONCOUNT; button++)
    {
      Serial.println(myappConfig.buttons[button].Topic);
      Serial.println(myappConfig.buttons[button].Value);
      Serial.println(myappConfig.buttons[button].LongpressValue);
      Serial.println(myappConfig.buttons[button].LongpressMillis);
    }
    has_valid_appconfig = true;
  }
  else
  {
    Serial.println(F("No Valid APP Config"));
    strcpy(myappConfig.firmwaretopic, "");
    for (uint8_t button = 0; button < BUTTONCOUNT; button++)
    {
      strcpy(myappConfig.buttons[button].Value, "");
      strcpy(myappConfig.buttons[button].getUrl, "");
      strcpy(myappConfig.buttons[button].getUrlLongpress, "");
      strcpy(myappConfig.buttons[button].LongpressValue, "");
      strcpy(myappConfig.buttons[button].LongpressMillis, "");
    }

  }
  print_wakeup_reason();


  if (myhostConfig.crc == crc((uint8_t *)&myhostConfig, sizeof(hostConfig)))
  {
    Serial.println(F("Valid Host Config readed"));
    Serial.print("crc: ");
    Serial.println(myhostConfig.crc);
    Serial.print("hostName: ");
    Serial.println(myhostConfig.hostName);
    Serial.print("realName: ");
    Serial.println(myhostConfig.realName);
    Serial.print("_ip: ");
    Serial.println(IPAddress(myhostConfig._ip).toString());
    Serial.print("_gw: ");
    Serial.println(IPAddress(myhostConfig._gw).toString());
    Serial.print("_sn: ");
    Serial.println(IPAddress(myhostConfig._sn).toString());
    Serial.print("mqttHost: ");
    Serial.println(IPAddress(myhostConfig.mqttHost).toString());
    Serial.print("mqttPort: ");
    Serial.println(myhostConfig.mqttPort);
    Serial.print("configTopic: ");
    Serial.println(myhostConfig.configTopic);



    /*
    mqttClient.setCleanSession(false); //wenn cleansession auf true kommt der folgende Fehler viel selterner
    
    Connecting to MQTT...
    Connected to MQTT.
    Session present: 1
    assertion "last_unsent->oversize_left >= oversize_used" failed: file "/Users/ficeto/Desktop/ESP32/ESP32/esp-idf-public/components/lwip/lwip/src/core/tcp_out.c", line 686, function: tcp_write
    abort() was called at PC 0x400e72c3 on core 1
    Backtrace: 0x4008d054:0x3ffc93a0 0x4008d285:0x3ffc93c0 0x400e72c3:0x3ffc93e0 0x40108271:0x3ffc9410 0x40133065:0x3ffc9480 0x400d5111:0x3ffc94c0 0x400d289a:0x3ffc94f0 0x401533a5:0x3ffc9680 0x400d66e7:0x3ffc96a0 0x40153519:0x3ffc96e0 0x400d6a96:0x3ffc9700 0x400d571a:0x3ffc9730 0x400d57aa:0x3ffc97a0 0x40133525:0x3ffc97c0 0x4013356d:0x3ffc9800 0x4013396e:0x3ffc9820 0x4008fbad:0x3ffc9850
    Rebooting...
    */



    mqttClient.onConnect(onMqttConnect);
    //mqttClient.onDisconnect(onMqttDisconnect);
    mqttClient.onSubscribe(onMqttSubscribe);
    mqttClient.onUnsubscribe(onMqttUnsubscribe);
    mqttClient.onMessage(onMqttMessage);
    mqttClient.onPublish(onMqttPublish);
    stationModeGotIpHandler = WiFi.onStationModeGotIP(stationModeGotIp);
  
    mqttClient.setServer(myhostConfig.mqttHost, myhostConfig.mqttPort);
    //wifiManager.autoConnect(myhostConfig.hostName);
    if (!setupWifiAndConfig(false))
    {
      Serial.println("failed to connect and hit timeout");
    }

  }
  else
  {
    Serial.println(F("Invalid Config readed"));
    Serial.println(myhostConfig.crc);
    strcpy(myhostConfig.hostName, DEFAULT_HOSTNAME);

    if (!setupWifiAndConfig(true))
    {
      Serial.println("failed to connect and hit timeout");
      delay(3000);
      //reset and try again, or maybe put it to deep sleep
      do_restart = true;
    }
  }

  if (shouldSaveConfig)
  {

  }

  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
  Serial.print("ESP Mac Address: ");
  Serial.println(WiFi.macAddress());
  Serial.print("Subnet Mask: ");
  Serial.println(WiFi.subnetMask());
  Serial.print("Gateway IP: ");
  Serial.println(WiFi.gatewayIP());
  //Serial.print("DNS: ");
  //Serial.println(WiFi.dnsIP());

/*
  float temp2 = temperatureReadFixed();
  Serial.print(temp2);
  Serial.println(" C (temp2)");
*/

  SPIFFS.begin();
  {
    Serial.println("SPIFFS contents:");

    Dir dir = SPIFFS.openDir("/");
    while (dir.next())
    {
      String fileName = dir.fileName();
      size_t fileSize = dir.fileSize();
      Serial.printf("FS File: %s, size: %s\n", fileName.c_str(), String(fileSize).c_str());
    }
    Serial.printf("\n");
  }

  if (MDNS.begin(myhostConfig.hostName))
  {
    Serial.print(F("MDNS responder started with hostname: "));
    Serial.println(myhostConfig.hostName);
    MDNS.addService("http", "tcp", 80);
    MDNS.addService(MDNSSERVICENAME, "tcp", 80);
    Serial.println(F("MDNS services added"));
  }

  server.reset();
  //w as www
  //server.serveStatic("/", SPIFFS, "/w/").setDefaultFile("index.html");

  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request) {
    handleFileRead(request);
  });
  scanWWWDir();

  // attach filesystem root at URL /fs
  server.serveStatic("/fs", SPIFFS, "/");
  server.on("/list", HTTP_GET, [](AsyncWebServerRequest *request) {
    handleFileList(request);
  });

  // attach AsyncWebSocket
  wscontrol.onEvent(wsEvent);
  server.addHandler(&wscontrol);

  server.onNotFound(onNotFound);

  // Simple Firmware Update Form
  server.on("/update", HTTP_GET, [](AsyncWebServerRequest *request) {
    request->send(200, "text/html", "<form method='POST' action='/update' enctype='multipart/form-data'><input type='file' name='update'><input type='submit' value='Update'></form>");
  });

  server.on("/update", HTTP_POST, [](AsyncWebServerRequest *request) {
    do_restart = !Update.hasError();
    AsyncWebServerResponse *response = request->beginResponse(200, "text/plain", do_restart?"OK":"FAIL");
    response->addHeader("Connection", "close");
    request->send(response); }, [](AsyncWebServerRequest *request, String filename, size_t index, uint8_t *data, size_t len, bool final) { updateFirmware(request, filename, index, data, len, final); });

  //upload a file to /upload
  server.on("/upload", HTTP_POST, [](AsyncWebServerRequest *request) { request->send(200); },
            [](AsyncWebServerRequest *request, String filename, size_t index, uint8_t *data, size_t len, bool final) {
              handleFileUpload(request, filename, index, data, len, final);
            });

  server.on("/upload", HTTP_DELETE, [](AsyncWebServerRequest *request) {
    handleFileDelete(request);
  });

  server.begin();

  Serial.println(F("Call setupApp"));
  setupApp();
  Serial.println(F("Call setupDebug"));
  setupDebug();
  Serial.println(F("Setup Ready"));

}

void loop()
{
  MDNS.update();  
  if (do_restart)
  {
    Serial.println("Rebooting...");
    delay(100);
    ESP.restart();
  }

  //delay(30000);
  if (force_config)
  {
    setupWifiAndConfig(true);
    doing_update = false;
  }

  if (millis() > (300 * 1000))
  {
    doing_update = false;
  }
  if (has_valid_appconfig) loopApp();
  loopDebug();
  /* if ((millis() % 20000) == 0)
  {
    Serial.println(F("End of main loop"));

  } */
}
