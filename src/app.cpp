#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <WiFiClient.h>


#include <FastLED.h>
#define DEBUGOUT // Comment to disable debug output

#define DBG_OUTPUT Serial

#ifdef DEBUGOUT
#define DEBUGLOG(...) DBG_OUTPUT.printf_P(__VA_ARGS__)
#else
#define DEBUGLOG(...)
#endif
#include "config.h"

extern appConfig myappConfig;
extern hostConfig myhostConfig;

WiFiClient client;
HTTPClient http;

buttonPinConfig buttonsConfig[] = {
    {BUTTON_1_PIN},
    {BUTTON_2_PIN}
    };

extern void sendUdpSyslog(String msgtosend);
extern void broadcastWS(String type, int value);
extern void sendDebugWS(String name, String type, String value);
extern uint16_t mqttPublish(String topic, String payload);

int last_mqtt_package_id = 0;
int last_publish_success_id = 0;

unsigned long apploopmillis = 0;

bool buttonPressed[BUTTONCOUNT] = {false};
//bool button2Pressed = false;
unsigned long buttonLongpressed[BUTTONCOUNT] = {0};
unsigned long buttonPause[BUTTONCOUNT] = {0};
unsigned long buttonDelay[BUTTONCOUNT] = {0};
unsigned long buttonFilter[BUTTONCOUNT] = {0};
#define BUTTONFILTERMILLIS 100
//unsigned long button2Longpressed = 0;

CRGB leds[BUTTONCOUNT];


uint16_t publishButton(uint8_t button, BUTTON_STATE value)
{
  String topicPrefix = String(myhostConfig.configTopic);
  String payload;
  uint16_t packetIdPub = 0;
  if (value != BUTTON_STATE::OFF)
  {
    if (value == BUTTON_STATE::SHORT_PRESSED)
    {
      payload = String(myappConfig.buttons[button].Value);
      packetIdPub = mqttPublish(String(myappConfig.buttons[button].Topic), payload);
      Serial.println(myappConfig.buttons[button].getUrl);
      http.begin(client, String(myappConfig.buttons[button].getUrl));
    }
    else if (value == BUTTON_STATE::LONG_PRESSED)
    {
      payload = String(myappConfig.buttons[button].LongpressValue);
      packetIdPub = mqttPublish(String(myappConfig.buttons[button].Topic), payload);
      http.begin(client, String(myappConfig.buttons[button].getUrlLongpress));
    }
/*     uint16_t packetIdPub = mqttPublish(
        String(myappConfig.buttons[button].Topic),
        payload); */
    int httpCode = http.GET();
    Serial.print("httpCode: ");
    Serial.println(httpCode);
    String httpPayload = http.getString();
    Serial.println(httpPayload);
    http.end();
    Serial.print("Publishing Button, packetId: ");
    Serial.println(packetIdPub);
    Serial.print(myappConfig.buttons[button].Topic);
    Serial.print(" => ");
    Serial.println(payload);
  }
  return packetIdPub;
}

int trigger_button_number = -1;
int trigger_button_value = -1;

int button(int number, int value)
{
  if (number < BUTTONCOUNT)
  {
    trigger_button_number = number;
    trigger_button_value = value;
  }
  return 0;
}

void setupApp()
{
  for (uint8_t button = 0; button < BUTTONCOUNT; button++)
  {

    pinMode(buttonsConfig[button].logic, INPUT_PULLUP);
    //pinMode(buttonsConfig[button].ledPower, OUTPUT);
    //digitalWrite(buttonsConfig[button].ledPower, HIGH);

  }
  //FastLED.addLeds<WS2812, DATA_PIN, RGB>(leds, NUM_LEDS);
  FastLED.addLeds<WS2812, LED_PIN_DATA, COLORSORT>(leds, BUTTONCOUNT);

/*   for (uint8_t button = 0; button < BUTTONCOUNT; button++)
  {
    if (digitalRead(buttonsConfig[button].logic) == 0)
    {
      buttonPressed[button] = true;
      FastLED.setBrightness(myappConfig.led_brightness);
      leds[button] = CRGB::Red;
      FastLED.show();
    }
  }
 */
}

void loopApp()
{
  for (uint8_t button = 0; button < BUTTONCOUNT; button++)
  {
    if (digitalRead(buttonsConfig[button].logic) == 1)
    {
      buttonDelay[button] = millis();
    }
    else
    {
      buttonFilter[button] = millis();
    }
    

    if (!buttonPressed[button] && 
    digitalRead(buttonsConfig[button].logic) == 0 &&
    (buttonPause[button] + 500) < millis() && (buttonDelay[button] + 3) < millis())
    {
      buttonPause[button] = millis();
      buttonPressed[button] = true;
      last_mqtt_package_id = publishButton(button, BUTTON_STATE::SHORT_PRESSED);
      FastLED.setBrightness(myappConfig.led_brightness);
      leds[button] = CRGB::Red;
      FastLED.show();

    }
    if (digitalRead(buttonsConfig[button].logic) == 1 && (buttonFilter[button] + BUTTONFILTERMILLIS) < millis())
    {
      buttonPressed[button] = false;
      buttonLongpressed[button] = millis();
    }
    else
    {
      unsigned int longPressMillis = atoi(myappConfig.buttons[button].LongpressMillis);
      if (longPressMillis < 100) longPressMillis = 30000;
      if ((buttonLongpressed[button] + longPressMillis) < millis())
      {
        buttonLongpressed[button] = millis();
        last_mqtt_package_id = publishButton(button, BUTTON_STATE::LONG_PRESSED);
        FastLED.setBrightness(myappConfig.led_brightness);
        leds[button] = CRGB::Blue;
        FastLED.show();
      }
    }
    if (buttonPressed[button] && digitalRead(buttonsConfig[button].logic) == 1 && (buttonFilter[button] + BUTTONFILTERMILLIS) < millis())
    {
      buttonPause[button] = millis();
      if (last_publish_success_id == last_mqtt_package_id)
      {
              leds[button] = CRGB::Green;
      }
      else
      {
              leds[button] = CRGB::Black;
      }
      FastLED.setBrightness(myappConfig.led_brightness);
      FastLED.show();
    }
  }
  if (trigger_button_number != -1)
  {
    BUTTON_STATE state_value = BUTTON_STATE::OFF;
    if (trigger_button_value == 1) 
    {
      state_value = BUTTON_STATE::SHORT_PRESSED;
    }
    else if (trigger_button_value == 2) 
    {
      state_value = BUTTON_STATE::LONG_PRESSED;
    }
    u16_t package_id = publishButton(trigger_button_number, state_value);
    if (package_id != 0){
      Serial.print("Publishing Button, packetId: ");
      Serial.println(package_id);
    }
    trigger_button_number = -1;
    trigger_button_value = -1;
  }
/*   if (millis() % 10000 == 0)
  {
    Serial.print("millis: ");
    Serial.println(millis());
    for (uint8_t button = 0; button < BUTTONCOUNT; button++)
    {
      Serial.print("Button: ");
      Serial.println(button);
      Serial.print("Pin (loop) ");
      Serial.println(digitalRead(buttonsConfig[button].logic));
    }
  }
 */
}