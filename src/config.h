#include <ESP8266WiFi.h>
#define CONFIG_CHAR_LENGTH 40

#define LED_BUILTIN 2

#define BUTTON_1_PIN 4
#define BUTTON_2_PIN 5
//#define BUTTON_1_LED_PWR 12
//#define BUTTON_2_LED_PWR 13

extern const char *version;
extern const char *buildtime;

struct hostConfig
{
  char hostName[CONFIG_CHAR_LENGTH];
  char realName[CONFIG_CHAR_LENGTH];
  ip_addr_t _ip;
  ip_addr_t _gw;
  ip_addr_t _sn;
  ip_addr_t mqttHost;
  uint16_t mqttPort;
  //char mqttUser[40];
  //char mqttPass[40];
  char configTopic[50];

  unsigned long crc;
};

struct logicDebugPin {
  const char *name;
  uint8_t pin;
};

typedef uint8 gpio_num_t;
struct buttonPinConfig
{
  gpio_num_t logic;
//  gpio_num_t ledPower;
};

#define BUTTONCOUNT 2

/* #ifndef _BUTTONSCONFIG_H
#define _BUTTONSCONFIG_H
buttonPinConfig buttonsConfig[] = {
    {4, 12},
    {5, 13}
    };
#endif
 */
#define COLORSORT GRB
#define LED_PIN_DATA 14
#define DEFAULT_BRIGHTNESS 32

#define MAXTOPICLENGTH 128
#define MAXPAYLOADLENGTH 30
#define MAXURLLENGHT 128

struct buttonTargetConfig
{
  char Topic[MAXTOPICLENGTH] = "";
  char Value[MAXPAYLOADLENGTH] = "";
  char LongpressValue[MAXPAYLOADLENGTH] = "";
  char getUrl[MAXURLLENGHT] = "";
  char getUrlLongpress[MAXURLLENGHT] = "";
  char LongpressMillis[MAXPAYLOADLENGTH] = "";
};

//#define LONGPRESSMILLIS 3000
struct appConfig
{
  buttonTargetConfig buttons[BUTTONCOUNT];
  char firmwaretopic[MAXTOPICLENGTH] = "";
  uint8_t led_brightness = DEFAULT_BRIGHTNESS;
  unsigned long crc = 0;
};


enum class BUTTON_STATE : uint8_t
{
  OFF = 0x00,
  SHORT_PRESSED = 0x01,
  LONG_PRESSED = 0x02,
};

